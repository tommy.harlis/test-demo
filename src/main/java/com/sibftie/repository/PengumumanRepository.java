package com.sibftie.repository;


import com.sibftie.model.Pengumuman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PengumumanRepository extends JpaRepository<Pengumuman, Long>
{
    public Pengumuman findPengumumanById(long id);
}
