package com.sibftie.repository;


import com.sibftie.model.Dokumen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DokumenRepository extends JpaRepository<Dokumen, Long>
{
    Dokumen findDokumenById(long id);
}
