package com.sibftie.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.sibftie.model.Aspirasi;
import com.sibftie.model.Dokumen;

public interface AspirasiRepository extends JpaRepository<Aspirasi, Long>{
	    Aspirasi findAspirasiById(long id);

}
