package com.sibftie.model;

import javax.persistence.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Created by solo on 01/05/2017.
 */
@Entity
@Table(name = "t_proposal")
public class Proposal
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Transient
    private MultipartFile fileProposal;

    public MultipartFile getFileProposal() {
        return fileProposal;
    }


    @Column(nullable = false)
    private String judulDokumen;

    @Column(nullable = false)
    private String pathDokument;

    @Column(nullable = false)
    private String status;

    @Column
    private Date tgl_dibuat;

    @Column
    private Date timeStamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "t_mahasiswa_id")
    private Mahasiswa mahasiswa;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJudulDokumen() {
		return judulDokumen;
	}

	public void setJudulDokumen(String judulDokumen) {
		this.judulDokumen = judulDokumen;
	}

	public String getPathDokument() {
		return pathDokument;
	}

	public void setPathDokument(String pathDokument) {
		this.pathDokument = pathDokument;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTgl_dibuat() {
		return tgl_dibuat;
	}

	public void setTgl_dibuat(Date tgl_dibuat) {
		this.tgl_dibuat = tgl_dibuat;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Mahasiswa getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(Mahasiswa mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public void setFileProposal(MultipartFile fileProposal) {
		this.fileProposal = fileProposal;
	}


}
