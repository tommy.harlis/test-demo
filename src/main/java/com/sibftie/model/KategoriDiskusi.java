package com.sibftie.model;

import javax.persistence.*;

/**
 * Created by solo on 01/05/2017.
 */
@Entity
@Table(name = "t_kategori_diskusi")
public class KategoriDiskusi
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String namaKategori;

    @Column
    private String deskripsiKategori;

    @ManyToOne
    @JoinColumn(name = "t_diskusi_id")
    private Diskusi diskusi;

}
