package com.sibftie.service;

import com.sibftie.model.Pengumuman;

import java.util.List;

public interface PengumumanService
{
    public void addPengumuman(Pengumuman p);
    public void updatePengumuman(Pengumuman p);
    public List<Pengumuman> getAllPengumuman();
    public Pengumuman getPengumumanById(long id);
}
