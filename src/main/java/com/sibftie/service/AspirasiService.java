package com.sibftie.service;

import java.util.List;

import com.sibftie.model.Aspirasi;

public interface AspirasiService {

	public void addAspirasi(Aspirasi a);
    public List<Aspirasi> getAllAspirasi();
    public Aspirasi getAspirasiById(long id);
    void hapus(long id);
}
