package com.sibftie.dao;

import com.sibftie.model.KategoriPengumuman;
import com.sibftie.repository.KategoriPengumumanRepository;
import com.sibftie.service.KetagoriPengumumanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KategoriPengumumanServiceImpl implements KetagoriPengumumanService
{
    @Autowired
    private KategoriPengumumanRepository kategoriPengumumanRepository;

    @Override
    public KategoriPengumuman getKategoriById(long id)
    {
        return kategoriPengumumanRepository.findById(id);
    }

    @Override
    public List<KategoriPengumuman> getAllKategoriPengumuman() {
        return kategoriPengumumanRepository.findAll();
    }
}
