package com.sibftie.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibftie.model.Aspirasi;
import com.sibftie.model.Proposal;
import com.sibftie.repository.AspirasiRepository;
import com.sibftie.service.AspirasiService;


@Service
public class AspirasiServiceImpl implements AspirasiService{
	
	private EntityManagerFactory emf;
	
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Autowired
    private AspirasiRepository aspirasiRepository;
	
	@Override
	public void addAspirasi(Aspirasi a) {
		aspirasiRepository.save(a);
		
	}

	@Override
	public List<Aspirasi> getAllAspirasi() {
		return aspirasiRepository.findAll();
	}

	@Override
	public Aspirasi getAspirasiById(long id) {
		return aspirasiRepository.findAspirasiById(id);
	}

	@Override
	public void hapus(long id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Aspirasi.class, id));
		em.getTransaction().commit();	
	}

}
