package com.sibftie.controller;
import com.sibftie.model.Akun;
import com.sibftie.model.MahasiswaJabatan;
import com.sibftie.service.AkunService;
import com.sibftie.service.KelasService;
import com.sibftie.service.MahasiswaJabatanService;
import com.sibftie.service.PengumumanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@ControllerAdvice
public class AutentikasiController
{
    @Autowired
    AkunService akunService;
    
    @Autowired
    private PengumumanService pengumumanService;

    @Autowired
    MahasiswaJabatanService mahasiswaJabatanService;



    @RequestMapping({"/", "/home", "/dashboard"})
    public String index(Model model)
    {
        return "pages/dashboard";
    }

    @RequestMapping("/user/login")
    public ModelAndView loginUser()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("daftarPengumuman", pengumumanService.getAllPengumuman());
        modelAndView.setViewName("pages/login");
        return modelAndView;
    }

    // for 403 access denied page
    @GetMapping("/403")
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        model.setViewName("pages/403");
        return model;

    }

    @ModelAttribute
    public void getCurrentUser(Model model)
    {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Akun a = akunService.getAkunByEmail(auth.getName());
            if(a != null) {
                MahasiswaJabatan mj = mahasiswaJabatanService.getMahasiswaJabatanByMahasiswa(a.getMahasiswa());
                model.addAttribute("mahasiswaJabatanLogged", mj);
            }
        }catch (Exception e)
        {
            e.toString();
        }

    }
}
