package com.sibftie.controller;
import com.sibftie.model.Akun;
import com.sibftie.model.Aspirasi;
import com.sibftie.model.Dokumen;
import com.sibftie.model.Mahasiswa;
import com.sibftie.model.MahasiswaJabatan;
import com.sibftie.model.Pengumuman;
import com.sibftie.model.Proposal;
import com.sibftie.service.AkunService;
import com.sibftie.service.AspirasiService;
import com.sibftie.service.FileStorageService;
import com.sibftie.service.ProposalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class AspirasiController {
	
	@Autowired
    private AspirasiService aspirasiService;
	
	@Autowired
    private AkunService akunService;
	
	@RequestMapping(value="/user/aspirasi/tambah-aspirasi", method = RequestMethod.GET)
    public ModelAndView tambahAspirasi(Mahasiswa mahasiswa, MahasiswaJabatan mahasiswaJabatan, Model model, Akun akun)
    {
        ModelAndView m = new ModelAndView();
        m.addObject("aspirasi", new Aspirasi());
        m.setViewName("/pages/user/tambah-aspirasi");
        return m;
    }


    @RequestMapping(value = "/user/aspirasi/tambah-aspirasi", method = RequestMethod.POST)
    public  ModelAndView saveAspirasi(@Valid @ModelAttribute("aspirasi") Aspirasi aspirasi, BindingResult bindingResultDokument,
                                     @ModelAttribute("mahasiswaJabatanLoged") MahasiswaJabatan mahasiswaJabatan)
    {

    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Akun a = akunService.getAkunByEmail(authentication.getName());
        System.out.println("====================================");
        System.out.println(authentication.getName());
        System.out.println("====================================");
        ModelAndView m = new ModelAndView();
        String error;
        error = cekAspirasiError(aspirasi.getIsiAspirasi());
        if(!error.equals(""))
        {
            m.addObject("aspirasi", new Aspirasi());
            m.addObject("aspirasiError", error);
            m.setViewName("/pages/user/tambah-aspirasi");
        }
        else
        {
	        aspirasi.setTgl_dibuat(new Date());
	        aspirasi.setMahasiswa(mahasiswaJabatan.getMahasiswa());
	        aspirasi.setMahasiswa(a.getMahasiswa());
	        aspirasiService.addAspirasi(aspirasi);
	        m.setViewName("redirect:/user/aspirasi/daftar-aspirasi");
	    }

        return m;
    }
    
    private String cekAspirasiError(String aspirasi)
    {
        String error = "";
        if(aspirasi.equalsIgnoreCase(""))
        {
           error += "Aspirasi harus diisi";
        }
        return error;
    }


    @GetMapping("/user/aspirasi/daftar-aspirasi")
    public ModelAndView daftarAspirasi()
    {
        ModelAndView m = new ModelAndView();
        m.setViewName("/pages/user/daftar-aspirasi");
        return m;
    }

    @GetMapping("/user/getAllAspirasi")
    public String getDaftarProposal(Model model)
    {
        model.addAttribute("aspirasis", aspirasiService.getAllAspirasi());
        return "/pages/user/aspirasi :: resultAspirasiList";
    }
    
    @RequestMapping(value="/aspirasi/hapus/{id}")
    public String hapus(@PathVariable long id){
	    aspirasiService.hapus(id);
	    return "redirect:/user/aspirasi/daftar-aspirasi";
    
    }
    
    @GetMapping("/aspirasi/daftar-aspirasi/detail/{id}")
    public ModelAndView deteailPengumuman(@PathVariable("id") long id)
    {
        ModelAndView m = new ModelAndView();
        m.addObject("aspirasi", aspirasiService.getAspirasiById(id));
        m.addObject("daftarAspirasi", aspirasiService.getAllAspirasi());
        m.setViewName("pages/user/detail-aspirasi");
        return m;
    }

}


