package com.sibftie.controller;
import com.sibftie.model.Akun;
import com.sibftie.model.Mahasiswa;
import com.sibftie.model.MahasiswaJabatan;
import com.sibftie.model.Pengumuman;
import com.sibftie.model.Proposal;
import com.sibftie.service.AkunService;
import com.sibftie.service.FileStorageService;
import com.sibftie.service.ProposalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class ProposalController {

	@Autowired
    private FileStorageService fileStorageService;

	@Autowired
    private ProposalService proposalService;
	
	@Autowired
    private AkunService akunService;

    @RequestMapping(value="/user/proposal/unggah-proposal", method = RequestMethod.GET)
    public ModelAndView unggahProposal(Mahasiswa mahasiswa, MahasiswaJabatan mahasiswaJabatan, Model model, Akun akun)
    {
        ModelAndView m = new ModelAndView();
        m.addObject("proposal", new Proposal());
        m.setViewName("/pages/user/unggah-proposal");
        return m;
    }


    @RequestMapping(value = "/user/proposal/unggah-proposal", method = RequestMethod.POST)
    public  ModelAndView saveProposal(@Valid @ModelAttribute("proposal") Proposal proposal, BindingResult bindingResultDokument,
                                     @ModelAttribute("mahasiswaJabatanLoged") MahasiswaJabatan mahasiswaJabatan)
    {

    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Akun a = akunService.getAkunByEmail(authentication.getName());
        System.out.println("====================================");
        System.out.println(authentication.getName());
        System.out.println("====================================");
        ModelAndView m = new ModelAndView();
        String error;
        System.out.println("======================================");
        System.out.println("Ukuran "+ proposal.getFileProposal().getSize());
        System.out.println("======================================");
        error = cekDokumenError(proposal.getFileProposal());
        if(!error.equals(""))
        {
            m.addObject("proposal", new Proposal());
            m.addObject("proposalError", error);
            m.setViewName("/pages/user/unggah-proposal");
        }else
        {
            String path = fileStorageService.store(proposal.getFileProposal(),"/publik/proposal/");
            proposal.setPathDokument(path);
//            String judulDokumen;
//			proposal.setJudulDokumen(judulDokumen);
            proposal.setStatus("Baru");
            proposal.setTgl_dibuat(new Date());
            proposal.setMahasiswa(mahasiswaJabatan.getMahasiswa());
            proposal.setTimeStamp(new Date());
            proposal.setMahasiswa(a.getMahasiswa());
            System.out.println("====================================");
//            System.out.println("Submit oleh "+ mahasiswaJabatan.getMahasiswa().getNamaLengkap());
            System.out.println("====================================");
            proposalService.addProposal(proposal);
            m.setViewName("redirect:/user/proposal/daftar-proposal");
        }

        return m;
    }

    private String cekDokumenError(MultipartFile file)
    {
        String error = "";
        if(!file.getContentType().toLowerCase().equals("application/pdf"))
        {
           error += "Format file "+file.getOriginalFilename()+" tidak didukung";
        }
        if(file.getSize() >= 31457280)
        {
            error+="\nUkuran file terlalu besar : "+file.getSize();
        }
        return error;
    }


    @GetMapping("/user/proposal/daftar-proposal")
    public ModelAndView daftarProposal()
    {
        ModelAndView m = new ModelAndView();
        m.setViewName("/pages/user/daftar-proposal");
        return m;
    }

    @GetMapping("/user/getAllProposal")
    public String getDaftarProposal(Model model)
    {
        model.addAttribute("proposals", proposalService.getAllProposal());
        return "/pages/user/proposal :: resultProposalList";
    }
    
    @RequestMapping(value="/proposal/hapus/{id}")
    public String hapus(@PathVariable long id){
    proposalService.hapus(id);
    return "redirect:/user/proposal/daftar-proposal";
    
    }
    
}
