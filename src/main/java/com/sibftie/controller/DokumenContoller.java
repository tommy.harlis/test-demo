package com.sibftie.controller;


import com.sibftie.model.Dokumen;
import com.sibftie.model.MahasiswaJabatan;
import com.sibftie.model.Pengumuman;
import com.sibftie.service.DokumenService;
import com.sibftie.service.FileStorageService;
import com.sibftie.service.MahasiswaService;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

@Controller
public class DokumenContoller
{

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private DokumenService dokumenService;
    
    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping("/user/dokumen/ungah-dokumen")
    public ModelAndView unggahDokumen()
    {
        ModelAndView m = new ModelAndView();
        m.addObject("dokumen", new Dokumen());
        m.setViewName("/pages/user/unggah-dokumen");
        return m;
    }


    @RequestMapping(value = "/user/dokumen/ungah-dokumen", method = RequestMethod.POST)
    public  ModelAndView saveDokumen(@Valid @ModelAttribute("dokumen") Dokumen dokumen, BindingResult bindingResultDokument,
                                     @ModelAttribute("mahasiswaJabatanLoged") MahasiswaJabatan mahasiswaJabatan)
    {

        ModelAndView m = new ModelAndView();
        String error;
        System.out.println("======================================");
        System.out.println("Ukuran "+ dokumen.getFileDokumen().getSize());
        System.out.println("======================================");
        error = cekDokumenError(dokumen.getFileDokumen());
        if(!error.equals(""))
        {
            m.addObject("dokumen", new Dokumen());
            m.addObject("dokumenError", error);
            m.setViewName("/pages/user/unggah-dokumen");
        }else
        {
            String path = fileStorageService.store(dokumen.getFileDokumen(),"/publik/dokumen/");
            dokumen.setPath(path);
            dokumen.setJmlSpam(0);
            dokumen.setTglDibuat(new Date());
            dokumen.setMahasiswa(mahasiswaJabatan.getMahasiswa());
            System.out.println("====================================");
//            System.out.println("Submit oleh "+ mahasiswaJabatan.getMahasiswa().getNamaLengkap());
            System.out.println("====================================");
            dokumenService.addDokumen(dokumen);
            m.setViewName("redirect:/user/dokumen/daftar-dokumen");
        }

        return m;
    }

    private String cekDokumenError(MultipartFile file)
    {
        String error = "";
        if(!file.getContentType().toLowerCase().equals("application/pdf"))
        {
           error += "Format file "+file.getOriginalFilename()+" tidak didukung";
        }
        if(file.getSize() >= 31457280)
        {
            error+="\nUkuran file terlalu besar : "+file.getSize();
        }
        return error;
    }


    @GetMapping("/user/dokumen/daftar-dokumen")
    public ModelAndView daftarDokumen()
    {
        ModelAndView m = new ModelAndView();
        m.setViewName("/pages/user/daftar-dokumen");
        return m;
    }

    @GetMapping("/user/getAllDokumen")
    public String getDaftarDokumen(Model model)
    {
        model.addAttribute("dokumens", dokumenService.getAllDokumen());
        model.addAttribute("mahasiswas",mahasiswaService.getAllMahasiswa());
        return "/pages/user/dokumen :: resultDokumenList";
    }
    
    
//    @RequestMapping(value="/dokumen/download", method=RequestMethod.GET)
//    @ResponseBody
//    public FileSystemResource downloadFile(@Param(value="id") Long id) {
//        Dokumen dokumen = dokumenService.getDokumenById(id);
//        return new FileSystemResource(new File(dokumen.getPath()));
//    }
    
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response, Dokumen dokumen) {
        try {
            DefaultResourceLoader loader = new DefaultResourceLoader();
            String path = dokumen.getPath();
            InputStream is = loader.getResource(""+path).getInputStream();
            IOUtils.copy(is, response.getOutputStream());
            response.setHeader("Content-Disposition", "attachment; filename=Accepted.pdf");
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }
    }
}
